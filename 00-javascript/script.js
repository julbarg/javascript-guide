console.log('Hola Mundo');

/*
    Functiones anomimas
*/
var add = function (a, b) {
    return a + b;
};

function add (a, b) {
    return a + b;
}

(function (a, b) {
    return a + b
})(6,7);

function imprimir () {
    console.log('Hola');
}

var imprimir = () => {
    console.log('Hola');
}

imprimir();

var add = (a, b) => {
    return a + b;
}

var i;
for (i = 0; i < )
