//IIFE
//Immediately Invoked Functions Expressions
(function (a, b) {
    return a + b;
})(9, 9);

var name = 'Julian';
function example() {
    let name = 'Andy';
    animal = 'Dog'
    console.log(name);
    return () => {
        let lastName = 'Barragan';

        return 'lastName: ' + lastName;
    }
}
console.log(name);
var t = example();
t();
console.log(lastName);

// ES2015 ES6
var variableGlobal;
let variableLocal;
const variableConstante;

function doSomeMath () {
    let a = 5;
    let b = 10;

    return function multiply () {
        return a * b;
    }
}
var theResult = doSomeMath();

function doSomeMath (func) {
    let a = 5;
    let b = 10;

    return func(a, b);
}
var theResult = doSomeMath((a, b) => {
    return a/b;
});





