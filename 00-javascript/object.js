var course1 = new Object();
course1.name = 'Algebra';
course1.horario = 'Lun-Vie';
course1.getName = function () {
    return this.name;
}
course1['last-name'] = 'Alberto';
// course1.last-name = 'Alberto';

var course2 = {
    name: 'Algebra',
    horario: 'Lun-Vie',
    getName: function () {
        return this.name;
    },
    'last-name': 'Alberto',
    students: ['Julian', 'Edgar', 'Carlos']
};

course1.name;

for(element in course2) {
    console.log(element + ': ' + course2[element])
}