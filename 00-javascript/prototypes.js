let name = {
    key1: 'value1',
    key2: 'value2'
};

function Arrangement (name, vase, quality = 1) {
    this.type = 'floral';
    this.name = name;
    this.vase = vase;
    this.quality = quality;
    this['color'] = 'blue';
    this.logItem = function () {
        console.log(this.name, this.type);
    }
};

let newItem = new Arrangement('name', 'vase', 90);
newItem.logItem();

class Item {
    constructor(name, lastName) {
        this.name = name;
        this.lastName = lastName;
    }
}

let item = new Item('name', 'lastName');

class Live extends Item {
    constructor(name, lastName, value) {
        super(name, lastName);
        this.value = value;
    }
}