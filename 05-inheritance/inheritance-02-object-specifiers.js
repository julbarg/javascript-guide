/**
 * OBJECT SPEIFIERS
 */

/**
 * It sometimes happens that a constructor is given a very large number of parameters
 * This can ne troublesome because it can be very difficult to remember the order of the
 * arguments
 * In such cases, it can be much friendlier if we write the constructor to accept a single
 * object specifier instead
 * That object contains the specification of the object to be constructed
 */

var myObject = maker(f, l, m, c, s);

var myObject = maker({
    first: f,
    last: l,
    middle: m,
    state: s,
    city: c
});

/**
 * The arguments can now be listed in any order, arguments can be left out if the
 * contructor is smart about defaults, and the code is much easier to read
 *
 * This can have a secondary benefit when working with JSON. JSON text can only describe data,
 * but sometimes the data represents an object, and it would be useful to associate the
 * data with its methods
 * This can be done trivially if the constructor takes an object to specifier because
 * we ca simply pass the JSON object to the constructor and it will return a fully
 * constituted object
 */