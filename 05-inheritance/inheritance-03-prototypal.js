/**
 * PROTOTYPAL
 */

/**
 * In a purely prototypal pattern, we dispense with classes
 * We focus instead on the objects
 * Prototypal inheritance is conceptually simpler than classical inheritance
 * a new object can inherit the properties of an old object
 * You start by making a useful object
 * You can then make many more objects that are like that one
 * Tje classification process of breaking an application down into a set of
 * nested abstract classes can be completely avoide
 */

var myMammal = {
    name: 'Hern the Mammal',
    get_name: function () {
        return this.name;
    },
    says: function () {
        return this.saying || '';
    }
};

/**
 * Once we have an object that we like, we can make more instances with the Object.create
 * method
 * We can then customize the new instances
 */
var myCat = Object.create(myMammal);
myCat.name = 'Henrietta';
myCat.saying = 'meow';
myCat.purr = function (n) {
    var i, s = '';
    for (i = 0; i < n; i += 1) {
        if (s) {
            s += '-';
        }

        s += 'r';
    }

    return s;
};

myCat.get_name = function () {
    return this.says() + ' ' + this.name + ' ' + this.says();
};

/**
 * This is differencial inheritance
 * By customizing a new object, we specify the differences from the object
 * on which it is based
 */

/**
 * Sometime is it useful for data structures to inherit from other data structures
 */
var block = function () {
    /**
     * Remember the current scope, Make a new scope that incluldes everything
     * from the current one
     */
    var oldScope = scope;
    scope = Object.create(scope);

    // Advance past the left curly brace
    advance('{');

    // Parse using the new scope
    parse(scope);

    // Advance past the right curly brace and discard the new scope, restoring the old one
    advance('}');
    scope = oldScope;
}
