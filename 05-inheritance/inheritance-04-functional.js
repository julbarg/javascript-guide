/**
 * FUNCTIONAL
 */

/**
 * One weakness of the inheritance patterns we have seen so far is that get no privacy
 * All properties of an object are visible
 * We get no private variables and no private methods
 *
 * A pattern have been adopted pretend privacy
 * If they have a property that they wish to make private, they give it an oddlooking name,
 * with the hope that other users of the code will pretend that they can not see the odd
 * looking members
 */

/**
 * We start by making a function that will produce objects
 * We will give it a name that starts with a lowercase letter because it will not require
 * the use of the new prefix
 */

/**
 * The functions contains four step
 * 1. It creates a new object. There are lots of ways to make an object.
 *    It can make an object literal, or it can call a constructor function with the new
 *    prefix, or it can use the Object.create method to make an instance from an existing
 *    object, or it can call any function that returns an object
 * 2. It optionally defines private instances variables and methods.
 *    Those methods will have priviliged access to the parameters and the vars defined
 *    of the function
 * 3. It augments the new object with methods. Those methods will have privileged access
 *    to the parameters and the vars defined in the second step
 * 4. It returns that new object
 */

var constructor = function (spec, my) {
    var that, other private instance variables;
    my = my || {};

    that = a new Object;

    Add priviliged method to that

    return that;
}

/**
 * The spec object contains all of the information that the constructor needs to make
 * a instance
 * The contents of the spec coul be copied into private variables or transformed by
 * other functions
 * Or the methods can access information form spec as they need it
 */

/**
 * The my object is a container of secrets that are shared by the constructors in
 * the inhertiance chain
 * The use of the my object is optional
 * If a my object is not passed in, the a my object is made
 */

/**
 * Next, decalte the private instance variables and private methods for the object
 * This is done by simply decalring variables
 * The variables and inner function of the constructor become the private members of the instance
 * The inner functions have access to spec and my that and the private variables
 *
 * Next, add the shared secrets to the my object
 */

 my.member = value;

/**
 * Now, we make a new object and assign to that
 * There are lots ways to make a new object
 *
 * We can call other function constructor, passing it a spec object and the my object
 * The my object allows the other constructor to share material taht we put into my
 * The other constructor may also put its own shared secrets into my so that our constructor
 * can take advantage of it
 */


var mammal = function (spec) {
    var that = {};

    that.get_name = function () {
        return spec.name;
    };

    that.says = function () {
        return spec.saying || '';
    }

    return that;
}

var cat = function (spec) {
    spec.saying = spec.saying || 'meow';
    var that = mammal(spec);
    that.purr = function (n) {
        var i, s = '';
        for (i = 0; i < n; i += 1) {
            if (s) {
                s += '-';
            }
            s += 'r';
        }

        return s;
    }
    that.get_name = function () {
        return that.says() + ' ' + spec.name + ' ' + that.says();
    }

    return that;
}

var myCat = cat({name: 'Henrietta'});

Function.prototype.method = function (name, func) {
    this.prototype[name] = func;

    return this;
}

/**
 * The functional patterns also gives us a way to deal with super methods
 * We will make a superior method that takes a method name and returns a function
 * that invokes that method
 * The function will invoke the original method even if the property is changed
 */

Object.method('superior', function (name) {
    var that = this,
    method = that[name];
    return function () {
        return method.apply(that, arguments);
    };
});

/**
 * Let's try it put on a coolcal that is just like cat except it has a cooler get_name
 * method that calls the super method.
 * It requires just a little bit of preparation
 * We will declare a super_get_name variable and assign it the result of invoking the
 * superior method
 */

var coolcat = function (spec) {
    var that = cat(spec),
        super_get_name = that.superior('get_name');
    that.get_name = function (n) {
        return 'like ' + super_get_name() + ' baby';
    }

    return that;
}

var myCoolCat = coolcat({name: 'Bix'});
var name = myCoolCat.get_name();

/**
 * The functional patter has a great deal of flexibility
 * It equires less effort tha the pseudoclassical pattern, and gives us better
 * encapsulation an information hiding and access to super methods
 *
 * If all of the state of an object is private, then the object is tamper-proof
 * Properties of the object can be replaced or deletem but the integrity of the object
 * is not compromised
 * If we crete an object in the functional style, and if all of the methods of the
 * object make no use of this or that, then the ibject is durable
 * A durable object is simply a collection of functions that act as capabilites
 *
 * A durable object cannot be compromised
 * Access t a durable object does not give an attacker the ability to access the
 * internal state of the object except as permitted by the methods
 */