/**
 * INHERITANCE
 */

/**
 * Inheritance is an important topic in most programming languages
 *
 * In the classical language, inheritance provides two uselful services
 * First, it is a form of code reuse
 * If a new class is mostly similar to an existing class, you only have to specify
 * the differences
 * Patterns of code reuse are extremely important because they have te potential to
 * significantly reduce the cost of software development
 * The other benefit of classical inheritance is that it includes the specification
 * of a system types
 * This mostly frees the programmer from having to write explicit casting operations,
 * which is a very good thing because when casting, the safety benefits of a type
 * system are lost
 */

/**
 * JavaScript, being loosely typed language, never casts
 * The lineage of an object is irrelevant
 * What matters about an object is what it can do, not what it is descended from
 */

/**
 * In classical language, objects are instances of classes, and a class can inherit from
 * another class
 * JavaScript is a prototypal language, which means that object inherit direclty from
 * other objects
 */