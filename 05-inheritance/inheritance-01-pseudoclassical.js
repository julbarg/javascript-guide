/**
 * PSEUDOCLASSICAL
 */

/**
 * JavaScript is conflicted about its prototypal nature
 * Its prototype mechanis is obscured by some complicated syntatic business that looks
 * vaguely classical
 * Instead of having object inherit direclty from other objects, an unnecessary level
 * of indirection is inserted such taht objects are produced by contructor functions
 */

/**
 * When a function object is created, the Function constructor taht produces the
 * function object runs some code like this
 */

this.prototype = {constructor: this};

/**
 * The new function object is given a prototype whose value is an object containing
 * a constructor property whose value is the new function object
 * The prototype object is the place where inherited traits are to be deposited
 * Every funtion get a prototype object because the language does not provide a way
 * of determining which functions are intended to be used as constructors
 * The constructor property is not useful
 * It is the prototype object that is important
 */

/**
 * When a function is invoked with the constructor invocation pattern using the new
 * prefix, this modifies the way in which the function is executed
 * If the new operator were a method instead of an operator, it could have been implemented
 * like this
 */

Function.prototype.method = function (name, func) {
    this.prototype[name] = func;

    return this;
}

Function.method('new', function () {
    /**
     * Create a new object that inhertis from the constructor's prototype
     */
    var that = Object.create(this.prototype);

    /**
     * Invoke the constructor, binding -tthis- to the new object
     */
    var other = this.apply(that, arguments);

    /**
     * If its return values isn't an object, substitute the new object
     */
    return (typeof other === 'object' && other) || that;
});

/**
 * We can define a constructor and augment its prototype
 */
var Mammal = function (name) {
    this.name = name;
}

Mammal.prototype.get_name = function () {
    return this.name;
}

Mammal.prototype.says = function () {
    return this.saying || '';
};

/**
 * Now, we can make an instance
 */
var myMammal = new Mammal('Herb the Mammal');
var name = myMammal.get_name();

/**
 * We can make another pseudoclass that inhertis from Mammal by defining its
 * constructor function and replacing its prototype with an instance of Mammal
 */
var Cat = function (name) {
    this.name = name;
    this.saying = 'meow'
};

/**
 * Replace Cat.prototype with a new instance of Mammal
 */
Cat.prototype = new Mammal();

/**
 * Augment the new prototype with purr and get_name methods
 */
Cat.prototype.purr = function (n) {
    var i, s = ''
    for (i = 0; i < n; i += 1) {
        if (s) {
            s += '-';
        }
        s += 'r';
    }

    return s;
};

Cat.prototype.get_name = function () {
    return this.says() + ' ' + this.name + ' ' + this.says();
};

var myCat = new Cat('Henrietta');
var says = myCat.say();
var purr = myCat.purr(5);
var name = myCat.get_name();

/**
 * The pseudoclassic pattern was intended to look sort of object-oriented, but it is
 * looking quite alien
 * We can hide sime of the ugliness by usin the method method and defining an inherits
 * method
 */

Function.method('inherits', function (Parent) {
    this.prototype = new Parent();

    return this;
});

/**
 * Our inherits and method method return thos, alloweing us to program in a cascade
 * style
 * We can now make our Cat with one statement
 */
var Cat = function (name) {
    this.name = name;
    this.saying = 'meow';
}.
    inherits(Mammal).
    method('purr', function (n) {
        var i, s = '';
        for (i = 0; i < n; i += 1) {
            if (s) {
                s += '-';
            }

            s += 'r';
        }

        return s;
    }).
    method('get_name', function () {
        return this.says() + ' ' + this.name + ' ' + this.says();
    });

/**
 * By hiding the prototype jazz, it now looks a bit less alien
 * But have we really imporve anything?
 * We now have constructor function that act like classes, but at the edges,
 * there may be surprising behavior
 * There is no privacy; all properties are public
 * There is no access to super methods
 *
 * Even worse, there is a serous hazard with the use of constructor functions
 * If you forget to include the new prefix when calling a constructor function,
 * then this wull not be bound to a new object
 * Sadly, this will be bound to the global variables
 * That is really bad
 * There is no compile warning, and there is no runtime warning
 *
 * This is a serous design error in the language
 * To mitigate this problem, there is a convetion that all constructors functions
 * are named with an initial capital, and that nothing else is spelled with an initial
 * capital.
 * This gives us a pryer that visual inspection can find a missing ne
 * A much better alternative is to not use new at all
 *
 * The pseudoclassical form can provide comfort programmers who are unfamiliar
 * with JavaScript, but it also hides the true nature of the language
 * The classically inspired notation can induce programmers to compose hierarchies that
 * are unneccesarily deep and complicated
 * Much of the complexity of class hierarchies is motivated by the constrans of sttic type checking
 * JavaScript is completely free of those constrains
 * In classical languages, class inheritance is the only form of code reuse
 * JavaScript has more and better options.
 */