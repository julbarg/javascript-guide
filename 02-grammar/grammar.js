/**
 * Whitespace
 *
 * Whitespace can take the form of formatting characters or comments
 */
var that = this;

/**
 * Comments
 *
 * JavaScript offers two forms of comments, block comments formed with \/* *\/ and
 * line-ending comments starting with // . Comments should be used liberally to
 * improve the readability of your programs. Take care that the comments always accu-
 * rately describe the code. Obsolete comments are worse than no comments.
 */

    /*
        var rm_a = /a*\/.match(s);
    */

/**
 * Names
 *
 * A name is a letter optionally followed by one or more letters, digits or underbars.
 * A name cannot be one of the these reserved words
 */
abstract
boolean break byte
case catch char class const continue
debugger default delete do double
else enum export extends
false final finally float for function
goto
if implements import in instanceof int interface
long
native new null
package private protected public
return
short static super switch synchronized
this throw transient true try typeof
var volatile void
while with

/**
 * Most of the reserved words in this list are not used in the language. The list does not
 * include some words that should have been reserved but were not, such as undefined ,
 * NaN , and Infinity . It is not permitted to name a variable or parameter with a reserved
 * word. Worse, it is not permitted to use a reserved word as the name of an object
 * property in an object literal or following a dot in a refinement.
 *
 * Names are used for statements, variables, parameters, property names, operators,
 * and labels.
 */

 /**
  * Numbers
  *
  * JavaScript has a single number type.
  * Internally, it is represented as 64-bit floating point.
  * Unlike most other programming languages, there is no separate integer type, so 1 and 1.0
  * are the same value.
  * This is a significant convenience because problems of overflow in short integers are
  * completely avoidedm and all you need to know about number is that it is a number
  */

  /**
   * Strings
   *
   * A string literal can be wrapped in single quotes or double quotes
   * It can contain zero or more characters
   * The \ (backslash) is the escape character
   * JavaScript was built at a time when Unicode was a 16-bit character set, so all characters
   * in JavaScript are 16 bits wide.
   *
   * JavaScript does not have a character type
   * To represent a character, make a string with just one character in it
   */

   "A" === "\u0041"

/**
 * The escape sequences allow for inserting characters into strings that are not
 * normally permitted, such as backslashes, quotes, and control characters
 * The \u convention allows for specifying character code points numerically
 *
 * String are immutable. Once it is made, a string can never be changed.
 */

/*
    Two strings containing exactly the same characters in the same order are considered
    to be the same string
*/

    'c' + 'a' + 't' === 'cat' //true

/*String have metods*/

    'cat'.toUpperCase() === 'CAT' // true

/**
 * Statements
 */
    /*
        Statements

        label name
            expression statement:
            disruptive statement;
            try statement
            if statement
            switch statement
            while statement
            for statement
            do statement
    */

    /* Disruptive statement
        break statement
        return statement
        throw statement
    */

    /* Block
        {statements}
    */

    /* if statement*/
        if(expression) {
            block;
        } else {
            block;
        }

    // Here are the falsy values:
        false
        null
        undefined
        The empty string ''
        The number 0
        The number NaN
    //All other values are truthy, including
        true
        'false'
        and all objects

    /* switch statement
        The switch statement performs a multiway branch.
        It compares the expression fir equality with all of the specified cases
        The expression can produce a number or a string
    */
    switch (expresion) {
        case 'uno':
            block;
            break;
        default:
            break;
    }

    /* while statement
        The while statement performs a simple loop. If the expression is falsy, then 
        the loop will break.
        While the expression is truthy, the block will be executed
    */
    while(expresion) {
        block;
    }

    /* for statement
    */
    for (initialization; condition; increment) {
        block;
    }

    for (variable in object) {
        block;
    }

    for (myVar in obj) {
        if (obj.hasOwnProperty(myVar)) {
            ...
        }
    }

    /* do statement */
    do
        block
    while (expression);

    /* try statment */
    try
        block
    catch(variable)
        block

    /* throw statement */
    throw expression;

    /* return statement */

    return expression;

    /* break statement */
    break;
    break label;

    /* expression statement
        An expression statement can either assgin values
        to one or more varaibles or members, inkoke a method
        delete a property from an object

        The = operator is used for assignment
        Do not confuse with the === equality operator
        The += operator can add or concatene
    */

/**
 * Expressions
 */
    /**
     * Operate precedence
     *  [] ()                   Refinement and invocation
     *  delete new typeof + - ! Unary operators
     * * / %                    Multiplication, division, remainder
     * + -                      Addition/concatenation, substraction
     * >= <= <>                 Inequality
     * === !==                  Equality
     * &&                       Logical and
     * ||                       Logical or
     * ?:                       Ternary
     */

    /**
     * The simplest expressions are a literal value (such as a string or number), a variable
     * a built-in value, an invocation expression preceded by new, a
     * refinement expression preceded by delete,
     * an expression wrapped in parentheses, an expression preceded by a prefix operator,
     */

    /**
     * prefix operator
     *
     *    typeof  type of
     *    +       to number
     *    -       negate
     *    !       logical not
     */

    /**
     * infix operator
     *    *
     *    /
     *    %
     *    +
     *    -
     *    >=
     *    <=
     *    >
     *    <
     *    ===
     *    !==
     *    ||
     *    &&
     */

    /**
     * invocation
     * Invocation causes the execution of a function value
     * The invocation operator is a pair of parentheses that
     * follow the funcion value.
     * The parentheses can contain arguments that will be delivered
     * to the function
     *
     * (expresion)
     */

/**
 * Literals
 *
 * Object literals are a convenient notation for specifying new objects
 * The names of the properties can be specified as names or as strings
 * The names are treated as literal names, not as variable names, so the names
 * of the properties are expressions
 */

    /**
     * literal
     *
     * number literal
     * string literal
     * object literal
     * array literal
     * function literal
     * regexp literal
     */

    /**
     * object literal
     */
    {
        name: expression,
        name2: expresion2
    }

    /**
     * array literal
     */
    [expression, expresion2]

    /**
     * regexp literal
     */
    /regexp choice/ g i m

/**
 * Functions
 */
    function name (parameter) {
        function body
    }






