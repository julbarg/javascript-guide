/**
 * CLOSURE
 */

/**
 * The good news about scope is that inner functions get access to the parameters and
 * variables of the functions they are defined within (with the exception of thos and
 * arguments).
 *
 */

/**
 * We made a myObject that had a value and increment method
 * Suppose we wanted to protect the value from unautorized changes
 *
 * Instead of initializing myObject with an object literal, we will initialize myObject
 * by calling a function that returns an object literal
 * The function defines a value variable
 * Thar variable is always available to the increment and getValue methods, but the
 * function's scope keeps it hidden from the rest of the program
 */

var myObject = (function () {
    var value = 0;

    return {
        increment: function (inc) {
            value += typeof inc === 'number' ? inc : 1;
        },

        getValue: function () {
            return value;
        }
    }
}());

/**
 * We are not assigning a funtion to myObject
 * We are assigning the result of invoking that function
 * Notice the () on the last line
 * The functions return an object containing two methods, and those methods continue
 * enjoy the privilege of access to the value variable
 */

/**
 * Create a maker function called quo. It makes an object with a get_status method
 * and a private status property
 */
var quo = function (status) {
    return {
        get_status: function () {
            return status;
        }
    };
}

// Make an instance of quo
var myQuo = quo('amazed');

document.writeln(myQuo.get_status());

/**
 * Define a function that sets a DOM node's color to yellow and then fades it to white
 */

var fade = function (node) {
    var level = 1;
    var step = function () {
        var hex = level.toString(16);
        node.style.backgroundColor = '#FFFF' + hex + hex;
        if (level < 15) {
            level += 1;
            setTimeout(step, 100);
        }
    };
    setTimeout(step, 100);
};

fade(document.body);

/**
 * BAD EXAMPLE
 */

var add_the_handlers = function (nodes) {
    var i;
    for (i = 0; i < nodes.length; i += 1) {
        nodes[i].onclick = function (e) {
            alert(i);
        };
    }
}

/**
 * BETTER EXAMPLE
 */
var add_the_handlers = function (nodes) {
    var helper = function (i) {
        return function (e) {
            alert(i);
        };
    };
}
var i;
for (i = 0; i < nodes.length; i += 1) {
    nodes[i].onclick = helper(i);
}