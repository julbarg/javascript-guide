/**
 * ARGUMENTS
 */

/**
 * A bounds parameter that is available to functions when they are invoked is the
 * argumnets array
 * It gives the function access to all arguments were suplied with the invocation,
 * including excess arguments that were not assigned to parameters
 * This makes it possible to write functions that take an unspecified number of 
 * parameters:
 */

// Make a function that ass a lot of stuff

/**
 * Note that defining the variable sum inside of the function does not inrefere with
 * the sum defined outside of the function.
 * The function only sees the inner one
 */

var sum = function () {
    var i, sum = 0;
    for (i = 0; i < arguments.length; i += 1) {
        sum += arguments[i]
    }
}

document.writeln(sum(4, 8, 15, 16, 23, 42)); // 108