/**
 * RETURN
 */

/**
 * When a function is invoked, ite begins execution with the first statement, and ends
 * when it hits the } that closes the function body
 * That causes the function to return control to the part of the program that invoked
 * the function
 *
 * The return statement can be used to cause the function to return early.
 * When return is executed, the function returns immediately without executing
 * the remaining statements
 *
 * A funtion always return a value. If the return is not specified, then undefined
 * is returned
 *
 * If the function was inkoved with the new prefix and the return value is not and
 * object, then this (the new object) is returned instead
 */