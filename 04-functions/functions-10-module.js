/**
 * MODULE
 */

/**
 * We can use functions and closure to make modules. A module is a function or object
 * that presents an interface but that hides its state and implementation
 * By using functions to produce module, we can almost completely elminate our
 * use of global variables, thereby mitigating one of JavaScript's wors features
 */

String.prototype.deentityify = function () {
    // The entity table. It maps entity names to characters

    var entity = {
        quot: '"',
        lt: '<',
        gt: '>'
    };

    // Return the deentityify

    return function () {
        /**
         * This is the deentityify method
         * It calls the string replace method, looking for substrings that start
         * with '&' and end with ';'
         * If the characters in between are in the entity table, then replace the
         * entity with the character from the table
         */

        return this.replace(/&([^&;]+);/g,
            function (a, b) {
                var r = entity[b];
                return typeof r == 'string' ? r : a;
            }
        );
    }
}();

/**
 * Notice the last line. We immediately invoke the function we just made with the ( )
 * operator. The invocation creates and returns the function that becomes the
 * deentityify method
 */
document.writeln('&lt;&quot;&gt;'.deentityify( ));

/**
 * The module pattern takes advantage of function scope and closure to create
 * relationships that are binding and private.
 * In this example only the deentityify method has access to the entity data structure
 */

/**
 * The general patterns of a module is a function that defines private variables
 * and functions; created privileged functions which, through closure, will have access to
 * the private variables and functions; and that returns the privileged functions or
 * stores the in an accessible place
 */

/**
 * Use of the modeule pattern can eliminate the use of global variables
 * It promotes information hiding and other good design practices
 * It is very effective in encapsulating applications and other singletons
 */

/**
 * It can also be used to produce objects that are secure
 * Let's suppose we want to make an object that produces a serial number
 */

var serial_maker = function () {
    /**
     * Produce an object that produces unique strings
     * A unique string is made up two parts: a prefix and a sequence number
     * The object comes with methods for setting the prefix and sequence number,
     * and a gensym method that produces unique strings.
     */

    var prefix = '';
    var seq = 0;
    return {
        set_prefix: function (p) {
            prefix = String(p);
        },
        set_seq: function (s) {
            seq = s;
        },
        gensym: function () {
            var result = prefix + seq;
            seq += 1;

            return result;
        }
    };
};

var seqer = serial_maker();
seqer.set_prefix('Q');
seqer.set_seq(1000);
var unique = seqer.gensym(); // unique is "Q1000"

/**
 * The methods do not make use of this or that.
 * As a result, there is no way to compromise the seqer
 * It is not possible to get or change the prefix or seq except as permitted by the methods
 * The seqer object is mutable, so the methods could be replaced, but that still does
 * not give access to its secrets
 * seqer is simply a collection of functions, and those functions are capabilities that
 * grant specific powers to use or modify the secret state
 *
 * If we passed seqer.gensym to a third party's function, that function woul be ble to
 * generate unique strings, but would be unable to change the prefix or seq
 */