/**
 * SCOPE
 */

/**
 * Scope in a programming language controls the visibility and lifetimes of variables
 * and parameters
 * This is an important service to the programmer because it reduces naming collisions
 * and provides automatic memory management
 */

var foo = function () {
    var a =3, b = 5;

    var bar = function () {
        var b = 7, c = 11;

        // At this point, a is 3, b is 7, and c is 11

        a += b + c;

        // At this point, a is 21, b is 7, and c is 11
    }

    // At this point, a is 3, b is 5, and c is not defined

    bar();

    // At this point, a is 21, b is 5;
}

/**
 * Most languages with C syntax have block scope
 * All variables defined in a block (a list of statments wrapped with curlt braces)
 * are not visible from outside of the block
 * The variables defined in a block can be released when execution of the block
 * is finished.
 *
 * Unfortunately, JavaScript does not have block scope even though its block syntax
 * suggest that it does.
 */