/**
 * RECURSION
 */

/**
 * A recursive function is a function taht calls itself, either directly or indirectly
 * Recursion is a powerful programming technique in which a problem is divided into a
 * set of similar subproblems, each solved with a trivial solution. Generally, a recursive
 * function calls itself to solve its subproblems
 */

var hanoi = function hanoi(disc, src, aux, dst) {
    if (disc > 0) {
        hanoi(disc -1, src, dst, aux);
        document.writeln('Move disc ' + disc + 'from ' + src + 'to' + dst);
    }
}

hanoi(3, 'Src', 'Aux', 'Dst');

/**
 * Recursive functions can be very effective in manipulating tree structures such
 * as the browser's Document Object Model (DOM)
 * Each recursive call is given a smaller piece of the tree to work on
 */

/**
 * Define a getElementByAttribute funtion
 * It takes an attribute name string and an optional
 * matching value
 * It calls walk_the_DOM, passing it a function that looks for
 * an attribute name un the node
 * The matching nodes are accumulated in a results array
 */

var getElementByAttribute = function (att, value) {
    var results = [];

    walk_the_DOM(document.body, function (node) {
        var actual = node.nodeType === 1 && node.getAttribute(att);

        if (typeof actual === 'string' && (actual === value || typeof value !== 'string')) {
            results.push(node);
        }
    });

    return results;
}

/**
 * Some languages offer the tail recursion optimization
 * This means that if a function returns the results of invoking itself recursively,
 * then the invocation is replaced with a loop, which can significantly speed things up
 * Unfortunately, JavaScript does not currently provide tail recursion optimization
 * Functions that recurse very deeply can fail by exhausting the return stack.
 *
 */

/**
 * Make a factorial function with tail recursion
 * It is tail recursove because it returns the result of calling itself
 *
 * JavaScript does not currently optimize this form
 */

var factorial = function factorial (i, a) {
    a = a || 1;

    if (i < 2) {
        return a;
    }

    return factorial(i-1, a * i);
};

document.writeln(factorial(4)); // 24