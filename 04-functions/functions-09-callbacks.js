/**
 * CALLBACKS
 */
request = prepare_the_request();
response = send_request_asynchronous(request);
display(response);

// better ways

request = prepare_the_request();
send_request_asynchronous(request, function (response) {
    display(response);
})