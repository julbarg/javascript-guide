/**
 * FUNCTIONS
 */

/**
 * The best thing about JavaScript is its implementation of functions.
 *
 * A function encloses a set of statements. Functions are the fundamental
 * modular unit of JavaScript
 *
 * They are used for code reuse, information hiding, and composition
 *
 * Functions are used to specify the behavior of objects
 *
 * Generally, the craft of programming is the factoring of a set of requirements into a set
 * of functions and data structures
 */

/**
 * FUNCTIONS OBJECTS
 */

/**
 * Functions in JavaScript are objects
 * Objects a re collections of name/value pairs having a hidden lin to a prototype object
 * Objects produced from object literals are linked to Object.proptype
 * Functions are linked to Function.proptype (which is itself linked to Object.proptype)
 * Every function is also created with two additional hidden properties: the functions's context
 * and the code that implements the function's behavior
 */

 /**
  * Every function object is also created with a prototype property.
  * Its value is an object with a constructor property whose value is the function
  * This is distinct from the hidden link to Function.prototype
  */

/**
 * Since functions are objects, they can be used like any other value
 * Functions can be stored in variables, objects and arrays
 * Functions can be passed as arguments to functions, and functions can be returned
 * Also since functions are objects, functions can have methods
 */

/**
 * The thing that is special about functions is thar they can be invoked
 */

/**
 * FUNCTION LITERAL
 */

 // Create a variable called add and store a function
 // in it that adds two numbers

var add = function (a, b) {
    return a + b;
};

/**
 * A function literal has four parts.
 * The first part is the reserved word function
 * The optional second part is the function's name
 * The function can use its name to call itself recursively
 * The name can also be used by debuggers and development tools to identify the function
 * If a funtion is not given a name, as shown in the previous example, it is said
 * to be anonymous
 *
 * The third part is the set of parameters of the function, wrapped in parentheses.
 * Within the parentheses is a set of zero or more parameter names, separated by commas
 * These names will be defined as variables in the function
 * Unlike ordinary variables, instead of being initialized to undefined, they will be initialized to the
 * arguments supplied when the function is invoked.
 *
 * The fourth part is a set of statements wrapped in curly braces
 * These statements are the body of the function
 * They are executed when the functions is invoked
 *
 * A funtion literal can appear anywhere that an expression can appear
 * Functions can be defined inside of other varaibles
 * An inner function of course has access to its parameters and variables of the function it is nested within
 * The function object created by a function literal contains a link to taht outer context
 * This is called closure.
 * This is the source of enormous expressive power
 */

/**
 * INVOCATION
 */

/**
 * Invoking a functions suspends the execution of the current function, passing control and parameters to the new function
 * In addition to the declared parameters, every function receive two additional parameters: this and arguments
 * The this parameter is very important in object oriented programming, and its values is determined by the invocation pattern.
 * There are four patters of invocation in JavaScript: the method invocation pattern, the constructor invocation pattern
 * and the apply invocation pattern
 */

/**
 * The Method Invocation Pattern
 */

// Create myObject. It has a value and an increment
// method. The increment method takes an optional parameter
// If the argument is not a number, then 1 is used as the default

var myObject = {
  value: 0,
  increment: function (inc) {
    this.value += typeof inc === 'number' ? inc : 1;
  }
};

myObject.increment();
document.writeln(myObject.value) // 1

myObject.increment(2);
document.writeln(myObject.value); // 3

/**
 * A method can use this to access the object so that it can retrieve values form the
 * object or modify the object
 * The binding of this to the object happens at invocation time
 * This very late binding makes functions that use this highly reusable
 * Methods that get their object context from this are called public methods
 */

/**
 * The Function Invocation Pattern
 */

/**
 * When a function is not the property of an object, then it is invoked as a funtion
 */
var sum = add(3, 4); // sum is 7

/**
 * When a function is invoked with this pattern, this is bound to the global object
 * This was a mistake in the design of the language.
 * Had the language been designed correctly, when the inner function is invoked,
 * this would still be bound to the this variable of the outer function.
 * A consequence of this error is that a method cannot employ an inner function to help
 * it do its works because the inner function does not share the method's access to the object
 * as its this is bound to the wrong value
 *
 * Fortunately, there is an easy workaround. If the method defines a variable and assigns it
 * the value of this, the inner function will have access to this through that variable
 * By convention, the name of that variable that:
 */

// Augment myObject with a double method


myObject.double = function () {
  var that = this; // Workaround

  var helper = function () {
    that.value = add(that.value, that.value);
  };

  helper(); // Invoke helper as a function
}

// Invoke double as a method
myObject.double();
document.writeln(myObject.value);

/**
 * The Constructor Invocation Pattern
 */

/**
 * JavaScript is a prototypal inheritance language.
 * That means that the objects can inherit properties directly from other objects
 * The language is class-free
 *
 * This is a radical departure from the current fashion
 * Most languages today are classical
 * Prototypal inheritance is powerfully expressive, but is not widely understood
 *
 * JavaScript itself is not confident in its protypal nature, so it offers an object-making
 * syntax that is reminiscent of the classical languages.
 *
 * Few classical programmers found prototypal inheritance to be acceptable, and classically
 * inspired syntax obscures the language's true prototypal nature. It is the worst of both worlds
 *
 * If a function is invoked with the new prefix, then a new object wil be created with a
 * hidden link to the value of the function's protoype member, and this will be bound to that
 * new object
 *
 * The new prefix also changes the behavior of the return statement
 * We will see more about that next
 */

// Create a constructor function called Quo.
// It makes an object with a status property

var Quo = function (string) {
  this.status = string;
}

// Give all instances of Quo a public method
// called get_status.
Quo.prototype.get_status = function () {
  return this.status;
}

// Make an instance of Quo.
var myQuo = new Quo('confused');

document.writeln(myQuo.get_status()); // confused

/**
 * Functions that are intended to be used with the new prefix are called constructors
 * By convention, they are kept in variables with a capitalized name.
 * If a constructor is called without the new prefix, very bad things can happen without
 * compile.time or runtime warning, so the capitalization convention is really important
 *
 * Use of this style of constructor function is not recommended
 * We eill see better alternatives in the next chapter
 */

/**
 * The Apply Invocation Patterns
 */

/**
 * Because JavaScript is a functional object-oriented language, 
 * functions can have methods
 *
 * The apply method lets us construct an array of arguments to use to invoke
 * a function
 * It also lets us choose the value of this
 * The apply metod takes two parameters. The first is the value taht should be bound to this
 * The second is an array of parametters
 */

// Make an array of 2 numbers and add them

var array = [3, 4];
var sum = add.apply(null, array); // sum is 7

// Make an object wit a status member
var statusObject = {
  status: 'A-OK'
};

/*
  status object does not inherit from Quo.prototype
  but we can invoke the get_status method on
  statusObject even though statusObject does not have
  a ger_status method
*/

var status = Quo.prototype.apply(statusObject); // status is 'A-OK'


