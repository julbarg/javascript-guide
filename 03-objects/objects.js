/**
 * Objects
 *
 * The simple types of JavaScript are numbers, strings booleans,
 * null and undefined. All other values are objects.
 * Numbers, strings and booleans are object-like in that they have methods,
 * but hter are immutable
 *
 * Objects in JavaScript are mutable keyed collections
 * In JavaScripts, arrays are objects, functions are objects, regular expressions are objects,
 * and of course, objects are objects
 *
 * An object is a contaianer of properties, where a property has a name and a value
 *
 * A property name can be any string, ncluding the empty string
 * A property value can be any JavScript value except for undefined
 *
 * Objects in JavaScript are class-free. There is no constraint on the names
 * of new properties or on the values of properties. Objects are useful for collectiong
 * and organizing data. Objects can contain other objects, so they can easly represent tree or
 * graph structures
 *
 * JavaScript includes a proptype linkage feature that allows one objecto to inherit the properties
 * of another. When used well, this can reduce object initialization time and memory consumptios
 */

 var empty_object = {};

 var stooge = {
     'first-name': 'Jerome',
     'last-name': 'Howard'
 };

 /** A property's value can be obtained from any expression, including another object literal.
  * Object can nest
  */

var flight = {
    airline: 'Oceanic',
    number: 815,
    departure: {
        IATA: 'SYD',
        time: '2014-09-22 14:55',
        city: 'Sydney'
    },
    arrival: {
        IATA: 'LAX',
        time: '2014-09-23 10:42',
        city: 'Los Angeles'
    }
};

/**
 * RETRIEVAL
 */

/**
 * Vales can be retrieved from an object by wrapping a string expression in a [ ] sufix
 * If the string expression is a string literal, and if it is a legal JavaScript name and
 * not reserved word, then the . notation can be used instead
 */

stooge['first-name'];   // 'Jerome'
flight.departure.IATA;  // 'SYD'

/**
 * The undefined value is produced if an attempt is made to retrieve a nonexistent meber
 */
stooge['middle-name'];   // undefined
flight.status;           // undefined
stooge['FIRST-NAME'];    // undefined

/**
 * The || operator can be used to fill in default values
 */
var middle = stooge['midle-name'] || '(none)';
var status = flight.status || 'unknown';

/**
 * Attempting to retrieve values from undefined will throw a TypeError esception
 * This can be guarded against with the && operator
 */
flight.equipment;                           //  undefined
flight.equipment.model;                     // throw "TyṕeError"
flight.equipment && flight.equipment.model  //  undefined

/**
 * UPDATE
 */

 /**
  * A value in an object can be updated by assignment.
  * If the property name already exists in the object, the property value is replaced
  */
 stooge['first-name'] = 'Jerome';

/**
 * If the objects does not already have the property name, the object is augmented
 */
stooge['middle-name'] = 'Lester';
stooge.nickname = 'Curly';

flight.equipment = {
    model: 'Boeing 777'
};
flight.status = 'overdue';

/**
 * REFERENCE
 */

 /**
  * Object are passed around by reference
  * They are never copied
  */
var x = stooge;
x.nickname = 'Bol';
var nick = stooge.nickname;
    // nick is 'Bol' because x and stooge are references to the same object

var a = {}, b = {}, c = {};
    //a, b, and c each refer to a different empty object
a = b = c = {};
    // a, b, and c all refer to the same empty object

/**
 * PROTOTYPE
 */

 /**
  * Every object is linked to a proptype object from which it can inherit properties
  * All objects created fomr object literals are linked to Object.proptype, an object
  * that comes standard with JavaScript
  */

if (typeof Object.create !== 'function') {
    Object.create = function (o) {
        var F = function () {};
        F.prototype = o;

        return new F();
    }
}

var another_stooge = Object.create(stooge);

/**
 * The prototype link has no effect on updating
 * When we make changes to an object, the object's proptype is not touched
 */
another_stooge['first-name'] = 'Harry';
another_stooge['middle-name'] = 'Moses';
another_stooge.nickname = 'Moe';

/**
 * REFLECTION
 */

 /**
  * It is easy to inspect an object to determine what properties it has by
  * attempting to retrieve the properties and examming the values obtained
  *
  * The typeof operator can be very helpful in determining the type of a property
  */

typeof flight.number;       // number
typeof flight.status;       // string
typeof flight.arrival;      // object
typeof flight.manifest;     // undefined

/**
 * Some care must be taken because any property chain can produce a value
 */

 typeof flight.toString;    // function
 typeof flight.constructor; // function

/**
 * Use the hasOwnProperty method, which returns true if the object has a particular property
 */
flight.hasOwnProperty('number');        // true
flight.hasOwnProperty('constructor')    // false

/**
 * ENUMERATION
 */

/**
 * The for in statement can loop over all of the property names in an object
 * The enumeration will include all of the properties so it is neccesary to filter
 * out the vaues you do not want
 *
 * The most common filters are the hasOwnProperty method and using typeof to exclude functions
 */

var name;
for (name in another_stooge) {
    if (typeof another_stooge[name] !== 'function') {
        console.log(name, another_stooge[name]);
    }
}

/**
 * There is no guarantee on the order of the names, so be prepared for the names
 * to appear in any order.
 * If you want to assure that the properties appear in a particular order,
 * it is best to avoid the for in statement entirely and instead make an
 * array containing the names of the properties
 */

var i;
var properties = [
    'first-name',
    'middle-name',
    'last-name',
    'profession'
];
for (i = 0; i <properties.length; i += 1) {
    document.writeln(properties[i] + ':' +
        another_stooge[properties[i]]);
}

/**
 * By using for instead of for in, we were able to get the properties we wanted
 * without worrying about what might be dredged up from the prototype chain,
 * and qw git them un the correct order
 */

 /**
  * DELETE
  */

/**
 * The delete operator can be used to remove a property from an object.
 * It will remove a property from the object if it has one.
 * It will not touch any of the objects in the prototype linkage
 *
 * Removing a property from an object may allow a property from the proptyp linkage to shine
 * through
 */

another_stooge.nickname; // Moe

// Remove nickname another_stooge, revealing
// the nickname of the prototype

delete another_stooge.nickname;

another_stooge.nickname // Curly

/**
 * GLOBAL ABATEMENT
 */

/**
 * JavaScript makes it easy to define global varibles that can hold all of the assets of
 * your application
 *
 * Unfortunately, global variuables weaken the resiliency of programs and should be avoided
 *
 * That variable then becomes the container for your application_
 */

var MYAPP = {};

MYAPP.stooge = {
    'first-name': 'Joe',
    'second-name': 'Howard'
};

MYAPP.flight = {
    airline: 'Oceanic',
    number: 815,
    departure: {
        IATA: 'SYD',
        time: '2014-09-22 14:55',
        city: 'Sydney'
    },
    arrival: {
        IATA: 'LAX',
        time: '2014-09-23 10:42',
        city: 'Los Angeles'
    }
};

/**
 * By reducing your global footprint to a single name, you significantly reduce the
 * chance of bad interactions with other applications, widgets, or libraries.
 * Your program also becomes easier to read because it is obvious that MYAPP.stooge refers to a top-level structure
 */


